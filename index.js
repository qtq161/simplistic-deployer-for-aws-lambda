const build = require('./src/build')
const deploy = require('./src/deploy')
const info = require('./src/info')

module.exports = params => {

	// Check all the config needed for the deployment is available.
	if (!params || !params.functionName || !params.zipPath || !params.region) {
		console.log(params)
		throw new Error('Params object with functionName, zipPath and region '
		+ 'is not provided or incomplete.')
	}

	if (process.env.SD_AWS_ACCESS_KEY_ID
		&& process.env.SD_AWS_SECRET_ACCESS_KEY) {
		params.awsAccessKeyId = process.env.SD_AWS_ACCESS_KEY_ID
		params.awsSecretAccessKey = process.env.SD_AWS_SECRET_ACCESS_KEY
	}

	info('Deployment configuration:')
	info({
		functionName: params.functionName,
		zipPath: params.zipPath,
		region: params.region,
		awsAccessKeyId:
			params.awsAccessKeyId ? 'From .deploy.env' : 'Using default',
		awsSecretAccessKey:
			params.awsSecretAccessKey ? 'From .deploy.env' : 'Using default',
	})

	// Create a zip file and upload it as a Lambda function.
	return build(params.zipPath)
	.then(() => deploy(params))
	.then(() => console.log(`Successfully deployed ${params.functionName}`))
	.catch(error => {
		console.log(error.message);
		if (/^Could not unzip uploaded file/.test(error.message)) {
			console.log('Note: you can upload the following zip file via the AWS '
			+ `Lambda Console web interface instead:
${params.zipPath}`)
		}
	})
}
