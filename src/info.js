const isVerbose = process.env.SD_VERBOSE
&& process.env.SD_VERBOSE.match(/(1|on|true)/i);

module.exports = message => {
	if (isVerbose) {
		console.log(message);
	}
}
