const fs = require('fs')
const AWS = require('aws-sdk')
const info = require('./info')

/**
 * Deploys the zip file to the given AWS Lambda function name.
 */
module.exports = params =>
new Promise((resolve, reject) => {
	const awsParams = {
		region: params.region
	}
	if (params.awsAccessKeyId && params.awsSecretAccessKey) {
		awsParams.accessKeyId = params.awsAccessKeyId
		awsParams.secretAccessKey = params.awsSecretAccessKey
	}
	const lambda = new AWS.Lambda(awsParams)
	info('Reading zip.')
	fs.readFile(params.zipPath, {
		encoding: null
	}, (err, zipContent) => {
		if (err) {
			return reject(err)
		}
		info('Uploading zip file as Lambda function.');
		lambda.updateFunctionCode({
			FunctionName: params.functionName,
			ZipFile: zipContent,
		}, (err, data) => {

			// Try and create the function if it did not exist.
			if (err && err.message.match(/^Function not found: arn/)) {
				if (!process.env.SD_ROLE_ARN) {
					return reject(new Error('Function did not exist and could not create '
					+ 'a new one as SD_ROLE_ARN (arn for a role for the Lambda '
					+ 'function) was not present.'))
				}

				info('Creating new Lambda function')
				lambda.createFunction({
					FunctionName: params.functionName,
					Runtime: process.env.SD_RUNTIME || 'nodejs8.10',
					Role: process.env.SD_ROLE_ARN,
					Code: {
						ZipFile: zipContent
					},
					Handler: process.env.SD_HANDLER || 'index.handler',
				}, (err, data) => {
					if (err) {
						return reject(err)
					}
					return resolve()
				})
			}
			else {
				if (err) {
					return reject(err)
				}
				return resolve()
			}
		})
	})
})
