const fs = require('fs');
const path = require('path');
const archiver = require('archiver');

const info = require('./info');

/**
 * Zips the src directory and the index.js file.
 */
module.exports = zipPath => new Promise((resolve, reject) => {
	const zipDir = path.dirname(zipPath);
	if (!fs.existsSync(zipDir)) {
		info(`Creating directory ${zipDir}`);
		fs.mkdirSync(zipDir);
	}

	// The zip file being created.
	const zipFile = fs.createWriteStream(zipPath);

	// Archive object for managing zip archive.
	const archive = archiver('zip');

	// Handle errors being thrown.
	archive.on('error', error => reject(error));

	// Handle warnings being thrown.
	archive.on('warning', warning => console.log(warning));

	// Resolve the promise when the archive is complete.
	archive.on('end', () => {
		info('Zip ended.');
		resolve()
	});

	archive.on('finish', () => {
		info('Zip finished.');
	});

	archive.on('close', () => {
		info('Zip closed.');
	});

	// Connect the zip archive manager to the zip file.
	archive.pipe(zipFile);

	// Add the ./index.js file or throw an error.
	if (fs.existsSync('./index.js')) {
		info('Adding ./index.js');
		archive.file('./index.js', {name: 'index.js'});
	}
	else {
		info('Aborting zip process');
		archive.abort();
		reject(new Error('Could not find ./index.js file'));
	}

	// Add the ./src folder if there is one.
	if (fs.existsSync('./src')) {
		info('Adding ./src/*');
		archive.directory('./src', 'src');
	}
	else {
		info('Skipping ./src/*');
	}

	// Add the ./node_modules folder if there is one.
	if (fs.existsSync('./node_modules')) {
		addNodeModules(archive);
	}

	console.log('Zipping index.js, ./src/* and ./node_modules/*');
	archive.finalize();
});

function addNodeModules(archive) {
	fs.readdirSync('./node_modules').forEach(item => {
		const relativePath = `./node_modules/${item}`;
		const zipItemPath = `node_modules/${item}`;
		if (!item.match(/^(\.|aws-sdk|simplistic-deployer-for-aws-lambda)/)) {
			info(`Adding ${relativePath}`);
			archive.directory(relativePath, zipItemPath);
		}
		else {
			info(`Skipping ${relativePath}`);
		}
	});
}
